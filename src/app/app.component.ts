import { Component } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'demo-project';

  constructor(private httpClient:HttpClient){}

  count:number=0;
  demoData!:any;
  ngOnInit(){
    this.httpClient.get('https://api.publicapis.org/entries').subscribe((response:any)=>{
     this.demoData=response.entries;
     console.log(this.demoData);
      this.count=response.count;
    })
  }
 
}
